<?php
/**
 * User: Oli Griffiths <oli@expandtheroom.com>
 * Date: 2/5/13
 * Time: 11:28 AM
 */

class ComMarkdownViewMarkdownHtml extends ComDefaultViewHtml
{
    public function __construct(KConfig $config)
    {
        parent::__construct($config);

        $this->getTemplate()->getFilter('alias')->addAlias(
            array('assets://' => (string) KRequest::base().'/site/component/markdown/resources/assets/'), KTemplateFilter::MODE_READ | KTemplateFilter::MODE_WRITE
        );
    }


    protected function _initialize(KConfig $config)
    {
        $config->append(array(
            'template_filters' => array('markdown')
        ));
        parent::_initialize($config);
    }


    public function render()
    {
        $this->path = KRequest::get('get.path','string', $this->path);

        $markdown = $this->getModel()->getRow();

        $this->file = $markdown->getFileContents($this->path);

        //Set the breadcrumbs
        $pathway = $this->getService('application')->getPathway();

        $path = explode('/', $this->file->path);

        foreach($path AS $i => $p){
            $name = $this->cleanName($p);
            $pathway->addItem($name, $i < count($path) -1 ? $this->getRoute('&path='.$p) : null);
        }

        return parent::render();
    }


    public function cleanName($name)
    {
        $name = preg_replace('#^[0-9\.]*\s*#','', $name);
        $name = preg_replace('#(\.md|\.markdown)$#','', $name);
        return $name;
    }
}