<?php
/**
 * User: Oli Griffiths <oli@expandtheroom.com>
 * Date: 2/12/13
 * Time: 3:32 PM
 */

class ComMarkdownAdapterBitbucket extends ComMarkdownAdapterDefault
{
    protected function _initialize(KConfig $config)
    {
        $config->append(array(
            'url' => 'https://api.bitbucket.org/1.0/repositories/$username/$repo/src/master'
        ));

        parent::_initialize($config);
    }
}