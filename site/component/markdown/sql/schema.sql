CREATE TABLE `jos_markdown_markdowns` (
  `markdown_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(20) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `username` varchar(30) DEFAULT NULL,
  `password` varchar(30) DEFAULT NULL,
  `owner` varchar(30) DEFAULT NULL,
  `repo` varchar(30) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `default_file` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`markdown_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;