<?php
/**
 * User: oli.griffiths
 * Date: 2/5/13
 * Time: 11:22 AM
  */

if(!preg_match('#\.(md|markdown)#', $file->path)):
    ?>
    <div class="alert alert-danger">
        <?= sprintf(@text('The requested file %s is not a valid markdown file'),$file->path) ?>
    </div>
    <?php
else :
?>

<article class="markdown">
    <ktml:markdown>
        <?= $file->content ?>
    </ktml:markdown>
</article>
<? endif ?>

<ktml:module position="left" content="prepend">
    <?= @template('list.html', array('contents' => $markdown->getRepoContents(null, true), 'title' => @text('Contents'))) ?>
</ktml:module>