<?php
/**
 * User: Oli Griffiths <oli@expandtheroom.com>
 * Date: 2/12/13
 * Time: 3:32 PM
 */

class ComMarkdownAdapterGithub extends ComMarkdownAdapterDefault
{
    protected function _initialize(KConfig $config)
    {
        $config->append(array(
            'url' => 'https://api.github.com/repos/$owner/$repo/contents'
        ));

        parent::_initialize($config);
    }

    protected function _requestRepoContents($path, $recursive)
    {
        $url = $this->getBaseUrl();
        $url = $url.$path;
        $url = rtrim($url,'/');

        $content = $this->_request($url);

        $json = json_decode($content);
        if(!$json){
            throw new UnexpectedValueException('No JSON response received: '.$content);
        }

        if(is_object($json) && $json->message){
            throw new UnexpectedValueException($json->message);
        }

        $return = (object) array(
            'dirs' => array(),
            'files' => array()
        );

        foreach($json AS $node)
        {
            //Clear off the root path
            if($this->_path) $node->path = preg_replace('#^'.preg_quote($this->_path).'/'.'#','',$node->path);

            if($node->type == 'file'){

                $return->files[] = (object) array('name' => $node->name, 'path' => $node->path, 'sha' => $node->sha);

            }else if($node->type == 'dir'){

                $dir = (object) array('name' => $node->name, 'path' => $node->path, 'files' => array(), 'dirs' => array(), 'sha' => $node->sha);

                if($recursive){
                    $contents = $this->_requestRepoContents(rawurlencode($node->path), $recursive);
                    $dir->dirs = $contents->dirs;
                    $dir->files = $contents->files;
                }

                $return->dirs[] = $dir;
            }
        }

        return $return;
    }

    protected function _requestFileContents($path)
    {
        $url = $this->getBaseUrl();
        $url .= trim($path,'/');

        $content = $this->_request($url);

        if(false === $json = json_decode($content)){
            throw new UnexpectedValueException('No JSON response received: '.$content);
        }

        if(!is_object($json)){
            throw new UnexpectedValueException('The returned data is of type '.gettype($json).', object expected');
        }

        if(is_object($json) && isset($json->message)){
            throw new UnexpectedValueException($json->message);
        }

        $file = (object) array('name' => $json->name, 'path' => $json->path, 'content' => '');

        if($json->type == 'file'){
            $file->content = base64_decode($json->content);
        }

        return $file;
    }
}