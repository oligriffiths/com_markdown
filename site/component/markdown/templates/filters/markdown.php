<?php
/**
 * User: Oli Griffiths <oli@expandtheroom.com>
 * Date: 2/7/13
 * Time: 11:19 AM
 */

class ComMarkdownTemplateFilterMarkdown extends KTemplateFilterAbstract implements KTemplateFilterWrite
{
    public function __construct(KConfig $config)
    {
        parent::__construct($config);

        //Load markdown class
        require_once dirname(dirname(dirname(__FILE__))) . '/libs/markdown/Markdown.php';
    }


    /**
     * Parse the text and filter it
     *
     * @param string Block of text to parse
     * @return KTemplateFilterWrite
     */
    public function write(&$text)
    {
        if(preg_match_all('#<ktml:markdown>(.*)</ktml:markdown>#siU', $text, $matches))
        {
            foreach($matches[1] AS $i => $match){
                $content = Michelf\Markdown::defaultTransform(trim($match));
                $text = str_replace($matches[$i][0], $content, $text);
            }
        }
    }
}