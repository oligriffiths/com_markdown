<?php
/**
 * User: Oli Griffiths <oli@expandtheroom.com>
 * Date: 2/12/13
 * Time: 3:32 PM
 */

class ComMarkdownAdapterDefault extends ComMarkdownAdapterAbstract
{
    protected $_url;
    protected $_username;
    protected $_password;
    protected $_repo;
    protected $_path;
    protected $_default_file;
    protected $_cache_prefix;

    public function __construct(KConfig $config)
    {
        parent::__construct($config);

        $this->_url = rtrim($config->url,'/');
        $this->_owner = $config->owner ?: $config->username;
        $this->_repo = $config->repo;
        $this->_username = $config->username ?: $config->owner;
        $this->_password = $config->password;
        $this->_path = trim($config->path,'/');
        $this->_default_file = $config->default_file;
        $this->_cache_prefix = $this->getService('loader')->getRegistry()->getCachePrefix().'-markdown-repo-content-';
    }

    protected function _initialize(KConfig $config)
    {
        $config->append(array(
            'url' => null,
            'owner' => null,
            'repo' => null,
            'username' => null,
            'password' => null,
            'path' => null,
            'default_file' => 'README.md'
        ));

        parent::_initialize($config);
    }


    public function getBaseUrl()
    {
        $url = $this->_url;
        $url = str_replace('$owner', $this->_owner, $url);
        $url = str_replace('$repo', $this->_repo, $url);
        $url = str_replace('$username', $this->_username, $url);
        $url = str_replace('$password', $this->_password, $url);
        return $url.'/'.($this->path ? $this->path.'/' : '');
    }

    public function getDefaultFile()
    {
        return $this->_default_file;
    }


    public function getRepoContents($path = null, $recursive = false)
    {
        $path = rawurlencode($path);

        if(extension_loaded('apc')){
            if($content = apc_fetch($this->_cache_prefix.md5($this->getBaseUrl().'/'.$path.'-'.(int) $recursive))){
                return $content;
            }
        }

        $content = $this->_requestRepoContents($path, $recursive);

        if(extension_loaded('apc') && (count($content->dirs) || count($content->files))){
            apc_store($this->_cache_prefix.md5($this->getBaseUrl().'/'.$path.'-'.(int) $recursive), $content);
        }

        return $content;
    }


    public function getFileContents($path = null)
    {
        if(!$path) $path = $this->_default_file;

        $path = rawurlencode($path);

        if(extension_loaded('apc')){
            if($content = apc_fetch($this->_cache_prefix.md5($this->getBaseUrl().'/'.$path))){
                return $content;
            }
        }

        $content = $this->_requestFileContents($path);

        if(extension_loaded('apc')){
            apc_store($this->_cache_prefix.md5($this->getBaseUrl().'/'.$path), $content);
        }

        return $content;
    }


    protected function _requestRepoContents($path, $recursive)
    {
        return (object) array(
            'dirs' => array(),
            'files' => array()
        );
    }


    protected function _requestFileContents($path)
    {
        return '';
    }

    protected function _request($url)
    {
        // create a new cURL resource
        $ch = curl_init();

        $options = array(
            CURLOPT_URL => $url,
            CURLOPT_HEADER => false,
            CURLOPT_RETURNTRANSFER => true
        );

        if($this->_password) $options[CURLOPT_USERPWD] = $this->_username . ":" . $this->_password;

        curl_setopt_array($ch, $options);
        $content = curl_exec($ch);
        curl_close($ch);

        return $content;
    }
}