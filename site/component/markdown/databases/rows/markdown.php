<?php
/**
 * User: Oli Griffiths <oli@expandtheroom.com>
 * Date: 2/5/13
 * Time: 11:25 AM
 */

class ComMarkdownDatabaseRowMarkdown extends ComMarkdownDatabaseRowDefault
{
    protected $_adapter;

    public function setData($data, $modified = true)
    {
        parent::setData($data, $modified);

        if($this->id){
            $this->getAdapter();
        }

        return $this;
    }


    public function getAdapter()
    {
        if($this->id){
            if(!$this->_adapter){
                $this->_adapter = $this->getService('com://site/markdown.adapter.'.$this->type, $this->getData());
                $this->mixin($this->_adapter);
            }

            return $this->_adapter;
        }
    }


    public function getRepoContents($path = null, $recursive = false)
    {
        $contents = $this->getAdapter()->getRepoContents($path, $recursive);
        $this->_countMarkdown($contents);
        return $contents;
    }


    protected function _countMarkdown(&$contents)
    {
        foreach($contents->files AS $i => $file){
            if(!preg_match('#\.(md|markdown)$#',$file->path)) unset($contents->files[$i]);
        }

        $contents->file_count = count($contents->files);

        foreach($contents->dirs AS $k => $dir){

            $this->_countMarkdown($dir);

            if(empty($dir->files)) unset($contents->dirs[$k]);
            else{
                foreach($dir->files AS $file){
                    if($dir->path == dirname($file->path) || $file->name == 'README.md' || $file->name == 'README.markdown'){
                        $dir->filepath = $file->path;
                        break;
                    }
                }
            }

            $contents->file_count += $dir->file_count;
        }
    }
}