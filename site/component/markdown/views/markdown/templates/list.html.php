<?php
/**
 * User: Oli Griffiths <oli@expandtheroom.com>
 * Date: 2/12/13
 * Time: 4:06 PM
 */

$level = isset($level) ? $level : 1;
$title = isset($title) ? $title : null;
?>
<? if($level == 1): ?>
    <style src="assets://css/markdown.css" />
<? endif ?>
<ul class="nav nav-list nav-list-nested level-<?= $level ?>">

    <? if($title): ?>
        <li class="nav-header"><?= $title ?></li>
    <? endif ?>

    <?php foreach($contents->dirs AS $dir):

        //Skip empty dirs
        if(!$dir->file_count) continue;
        ?>
        <li>
            <? if(isset($dir->filepath)): ?>
                <a href="<?= @route('&path='.$dir->filepath) ?>">
            <? endif ?>
                <?= ucfirst($this->getView()->cleanName($dir->name)) ?>
            <? if(isset($dir->filepath)): ?>
                </a>
            <? endif ?>
            <? if($dir->file_count): ?>
                <?= @template('list.html', array('contents' => $dir, 'level' => $level + 1, 'title' => null)); ?>
            <? endif ?>
        </li>
    <? endforeach ?>

    <? foreach($contents->files AS $file): ?>
        <li class="<?= $path == $file->path || (!$path && $file->path == $markdown->getDefaultFile()) ? 'active' : '' ?>">
            <a href="<?= @route('&path='.($file->path == $markdown->getDefaultFile() ? '' : $file->path)) ?>">
                <?= ucfirst($this->getView()->cleanName($file->name)) ?>
            </a>
        </li>
    <? endforeach ?>
</ul>